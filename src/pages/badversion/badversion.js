import React, { Component } from 'react';
import Aside from "../../component/aside/aside"
import MainContainer from "../../component/container/main"
import Http from "../../component/http/http"

class BadVersion extends Component {
  
    constructor() {
        super();
        this.state = {
            how_many_version : 0,
            which_is_bad_version : 0,
            how_many_call : 0,
            which_is_bad_version_res : 0
        };
    }

    OnChange(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    Submit() {
        Http.Post("api/first-bad-version", {
            how_many_version : parseInt(this.state.how_many_version),
            which_is_bad_version : parseInt(this.state.which_is_bad_version),
        }, (res) => {
            if (res.code == 200) {
                this.setState({
                    how_many_call : res.data.how_many_call,
                    which_is_bad_version_res : res.data.which_is_bad_version
                })
            } else {
                this.setState({
                    output : res.data.error
                })
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                    <Aside/>
                    <MainContainer title="First Bad Version" sub_title="Search the first bad version">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Input</h4>
                                            <div className="form-group row">
                                                <label className="mt-2 col-12">How Many Version</label>
                                                <div className="col-12 col-sm-12">
                                                    <input className="form-control" type="number" id="how_many_version" onChange={ (e) => this.OnChange(e) }/>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="mt-2 col-12">Which is bad version</label>
                                                <div className="col-12 col-sm-12">
                                                    <input className="form-control" type="number" id="which_is_bad_version" onChange={ (e) => this.OnChange(e) }/>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <div className="col-12 col-sm-12">
                                                    <button className="btn btn-primary" onClick={ () => this.Submit() }>Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Output</h4>
                                            <p>How Many Call : {this.state.how_many_call}</p>
                                            <p>Which is bad version : {this.state.which_is_bad_version_res}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </MainContainer>
            </React.Fragment>
        )
    }
}

export default BadVersion;