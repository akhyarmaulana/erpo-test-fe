import React, { Component } from 'react';
import Aside from "../../component/aside/aside"
import MainContainer from "../../component/container/main"
import Http from "../../component/http/http"

class Reversestring extends Component {
  
    constructor() {
        super();
        this.state = {
            input : "",
            array_length : "",
            result_array : [],
        };
    }

    OnChange(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    Submit() {
        let input = this.state.input;
        let array_int = [];
        let array_string = input.split(",");        
        array_string.map(val => {
            array_int.push(parseInt(val))
        })
        Http.Post("api/duplicate-array", {
            input : array_int,
        }, (res) => {
            if (res.code === 200) {
                this.setState({
                    array_length : res.data.array_length,
                    result_array : res.data.result_array
                })
            } else {
                this.setState({
                    output : res.data.error
                })
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                    <Aside/>
                    <MainContainer title="Duplicates Array" sub_title="Remove Duplicates from Sorted Array">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Input</h4>
                                            <div className="form-group row">
                                                <label className="mt-2 col-12">Input</label>
                                                <div className="col-12 col-sm-12">
                                                    <input className="form-control" type="text" placeholder="Sparate with comma (,), example : 0,0,3,3,4,4,5,5 " id="input" onChange={ (e) => this.OnChange(e) }/>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <div className="col-12 col-sm-12">
                                                    <button className="btn btn-primary" onClick={ () => this.Submit() }>Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Output</h4>
                                            <p>Array Length : {this.state.array_length}</p>
                                            <p>Array Result : [{this.state.result_array.join()}]</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </MainContainer>
            </React.Fragment>
        )
    }
}

export default Reversestring;