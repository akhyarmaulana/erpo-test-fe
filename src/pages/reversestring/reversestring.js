import React, { Component } from 'react';
import Aside from "../../component/aside/aside"
import MainContainer from "../../component/container/main"
import Http from "../../component/http/http"

class Reversestring extends Component {
  
    constructor() {
        super();
        this.state = {
            input : "",
            output : "",
            k : 0
        };
    }

    OnChange(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    Submit() {
        Http.Post("api/reverse", {
            input_string : this.state.input,
            k : parseInt(this.state.k)
        }, (res) => {
            if (res.code === 200) {
                this.setState({
                    output : res.data.output
                })
            } else {
                this.setState({
                    output : res.data.error
                })
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                    <Aside/>
                    <MainContainer title="Reverse String" sub_title="Reverse String">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Input</h4>
                                            <div className="form-group row">
                                                <label className="mt-2 col-12">Input String</label>
                                                <div className="col-12 col-sm-12">
                                                    <textarea className="form-control" row="3" placeholder="Input string" id="input" onChange={ (e) => this.OnChange(e) }></textarea>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="mt-2 col-12">K</label>
                                                <div className="col-12 col-sm-12">
                                                    <input className="form-control" type="number" placeholder="K" id="k" onChange={ (e) => this.OnChange(e) }/>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <div className="col-12 col-sm-12">
                                                    <button className="btn btn-primary" onClick={ () => this.Submit() }>Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Output</h4>
                                            <p className="output">{this.state.output}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </MainContainer>
            </React.Fragment>
        )
    }
}

export default Reversestring;