import React, { Component } from 'react';
import Aside from "../../component/aside/aside"
import MainContainer from "../../component/container/main"
import Http from "../../component/http/http"

class Reversestring extends Component {
  
    constructor() {
        super();
        this.state = {
            input : "",
            output : ""
        };
    }

    OnChange(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    Submit() {
        let input = this.state.input;
        Http.Post("api/number-one-bit", {
            input : parseInt(input),
        }, (res) => {
            if (res.code === 200) {
                this.setState({
                    output : res.data.output
                })
            } else {
                this.setState({
                    output : res.data.error
                })
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                    <Aside/>
                    <MainContainer title="Count The '1' Bit" sub_title="Count bit 1 from uint value">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Input</h4>
                                            <div className="form-group row">
                                                <label className="mt-2 col-12">Input</label>
                                                <div className="col-12 col-sm-12">
                                                    <input className="form-control" type="text" placeholder="Input" id="input" onChange={ (e) => this.OnChange(e) }/>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <div className="col-12 col-sm-12">
                                                    <button className="btn btn-primary" onClick={ () => this.Submit() }>Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="card">
                                        <div className="card-body p-4">
                                            <h4>Output</h4>
                                            <p>{this.state.output}</p>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </MainContainer>
            </React.Fragment>
        )
    }
}

export default Reversestring;