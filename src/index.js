import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import ReverseString from "./pages/reversestring/reversestring"
import SortArray from "./pages/sortarray/sortarray"
import BadVersion from "./pages/badversion/badversion"
import CountBit from "./pages/countbit/countbit"

const Root = () => (
    <Router>
        <React.Fragment>
            <Route exact={true} path="/" component={ReverseString}></Route>
            <Route exact={true} path="/remove-duplicate-array" component={SortArray}></Route>
            <Route exact={true} path="/first-bad-version" component={BadVersion}></Route>
            <Route exact={true} path="/count-one-bit" component={CountBit}></Route>
        </React.Fragment>
    </Router>
);

ReactDOM.render(<Root />, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
