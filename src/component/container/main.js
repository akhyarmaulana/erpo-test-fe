import React, { Component } from 'react';
import "./container.css"

class Main extends Component {
  
    constructor() {
        super();
        this.state = {
        };
    }

    componentDidMount() {
        if (this.props.title) {
            this.setState({
                title : this.props.title,
            });
        } else {
            this.setState({
                title : "Sample Page",
            });
        }

        if (this.props.sub_title) {
            this.setState({
                sub_title : this.props.sub_title
            })
        } else {
            this.setState({
                sub_title : "Sample page for clear template"
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <div id="content" className="app-content box-shadow-z0" role="main">
                    <div className="app-header white box-shadow">
                        <div className="navbar navbar-toggleable-sm flex-row align-items-center">
                        <a data-toggle="modal" data-target="#aside" className="hidden-lg-up mr-3">
                            <i className="material-icons">&#xe5d2;</i>
                        </a>

                        <div className="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"></div>
                            <div className="container" style={{width:"100%"}}>
                                <div className="row" style={{width:"100%"}}>
                                    <div className="col-6">
                                        <h4 className="mb-0 _300">{ this.state.title }</h4>
                                        <small className="text-muted">{ this.state.sub_title }</small>    
                                    </div>
                                    <div className="col-6 p-2">
                                        <ul className="nav navbar-nav ml-auto flex-row float-right">
                                            <li className="nav-item dropdown pos-stc-xs">
                                                <div className="nav-link mr-2" data-toggle="dropdown">
                                                <i className="material-icons">&#xe7f5;</i>
                                                <span className="label label-sm up warn">3</span>
                                                </div>
                                                
                                            </li>
                                            <li className="nav-item dropdown">
                                                <div className="nav-link p-0 clear" data-toggle="dropdown">
                                                <span className="avatar w-32">
                                                    <img src="../assets/images/a0.jpg" alt="..."/>
                                                    <i className="on b-white bottom"></i>
                                                </span>
                                                </div>
                                            </li>
                                            <li className="nav-item hidden-md-up">
                                                <div className="nav-link pl-2" data-toggle="collapse" data-target="#collapse">
                                                <i className="material-icons">&#xe5d4;</i>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="app-footer">
                        <div className="p-2 text-xs">
                            <div className="pull-right text-muted py-1">
                                &copy; Copyright <strong>Swish</strong> <span className="hidden-xs-down">- Built with Love v1.1.3</span>
                            </div>
                        </div>
                    </div>
                    <div className="main-container-body">
                    { this.props.children }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Main;