import Config from "../config/config"

const Http = {
    Get : (url, callback) => {
        fetch(Config.BaseUrl + url, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + Config.Token,
			},
		}).then(response => {
			return response.json()
		}).then(json => {
			callback(json)
		}).catch(error => {
            if (Config.Debug) {
                console.log("Here the error stuff", error);
            }
		});
	},
	Upload : (binary, type, callback) => {
		fetch(Config.BaseUrl + '/api/filesystem/upload', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + Config.Token,
			},
			body: binary
		}).then(response => {
			return response.json()
		}).then(json => {
			callback(json)
		}).catch(error => {
            if (Config.Debug) {
                console.log("Here the error stuff", error);
            }
		});
	},
	Post : (url, param, callback) => {
		fetch(Config.BaseUrl + url, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + Config.Token,
			},
			body: JSON.stringify(param)
		}).then(response => {
			return response.json()
		}).then(json => {
			callback(json)
		}).catch(error => {
            if (Config.Debug) {
                console.log("Here the error stuff", error);
            }
		});
	}
}

export default Http