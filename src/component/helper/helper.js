const Helper = {
    Rupiah : (str) => {
        str = String(str);
        str = str.replace(/(\d)(?=(\d{3})+$)/g, "$1.");
        return "Rp&nbsp;" + str;
    },
    Slug : (Text) => {
        return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
    },
    Listener : (callback) => {
        setInterval(() => {
            if (localStorage.getItem('listener') === "active") {
                callback();
                localStorage.setItem('listener', 'disable');
            }
        }, 200)
    },
    Call: () => {
        localStorage.setItem('listener', 'active');
    }
}

export default Helper