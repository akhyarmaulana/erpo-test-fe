import React, { Component } from 'react';
import { BrowserRouter as Route, Link } from 'react-router-dom';

class Aside extends Component {
  
    constructor() {
        super();
        this.state = {
        };
        console.log(Route.id)
    }

    render() {
        return (
            <React.Fragment>
                <div id="aside" className="app-aside modal nav-dropdown">

                    <div className="left navside dark dk" data-layout="column">
                        <div className="navbar no-radius">

                            <a href="/" className="navbar-brand">
                                <div ui-include="'../assets/images/logo.svg'"></div>
                                <img src="../assets/images/logo.png" alt="." className="hide" />
                                <span className="hidden-folded inline">ERPO TEST</span>
                            </a>

                        </div>
                        <div className="hide-scroll" data-flex>
                            <nav className="scroll nav-light">

                                <ul className="nav">
                                    <li className="nav-header hidden-folded">
                                        <small className="text-muted">General</small>
                                    </li>
                                    <li>
                                        <Link to="/">
                                            <span className="nav-icon">
                                                <i className="material-icons">&#xe3fc;
                                                    <span ui-include="'../assets/images/i_0.svg'"></span>
                                                </i>
                                            </span>
                                            <span className="nav-text">Reverse String</span>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/remove-duplicate-array">
                                            <span className="nav-icon">
                                                <i className="material-icons">&#xe8f0;
                                                    <span ui-include="'../assets/images/i_2.svg'"></span>
                                                </i>
                                            </span>
                                            <span className="nav-text">Remove Duplicates from Sorted Array</span>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/first-bad-version">
                                            <span className="nav-icon">
                                                <i className="material-icons">&#xe8f0;
                                                    <span ui-include="'../assets/images/i_2.svg'"></span>
                                                </i>
                                            </span>
                                            <span className="nav-text">First Bad Version</span>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/count-one-bit">
                                            <span className="nav-icon">
                                                <i className="material-icons">&#xe8d2;
                                                    <span ui-include="'../assets/images/i_3.svg'"></span>
                                                </i>
                                            </span>
                                            <span className="nav-text">Number of 1 Bits</span>
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div className="b-t">
                            <div className="nav-fold">
                                <a href="profile.html">
                                    <span className="pull-left">
                                        <img src="../assets/images/a0.jpg" alt="..." className="w-40 img-circle" />
                                    </span>
                                    <span className="clear hidden-folded p-x">
                                        <span className="block _500">Jean Reyes</span>
                                        <small className="block text-muted"><i
                                                className="fa fa-circle text-success m-r-sm"></i>online</small>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    </div>
            </React.Fragment>
        );
    }
}

export default Aside;